exports.config = {
    tests: './todomvc-tests/**/unit.js',
    output: './output',
    helpers: {
        Puppeteer: {
            url: 'http://localhost',
            waitForTimeout: 5000,
            waitForNavigation: 'networkidle0',
            waitForAction: 0,
            show: false,
            chrome: {
                args: [
                    '--no-sandbox',
                    '--disable-setup-sandbox',
                    '--disable-web-security',
                    '--land=ru-RU,ru',
                    '--use-fake-ui-for-media-stream',
                ],
            }
        }
    },

    REST: {},

    CustomHelper: {
        require: './todomvc-tests/helpers/custom.helper.js'
    },

    include: {
        TodosPage: './todomvc-tests/pages/todos.page.js'
    },
    bootstrap: null,
    mocha: {},
    name: 'codecept demo tests'
}
